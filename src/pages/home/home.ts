import { Component } from '@angular/core';
import { CameraProvider } from '../../providers/camera/camera';
import { AlertController } from 'ionic-angular';
import { WebserviceProvider } from '../../providers/webservice/webservice';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    public cameraProv: CameraProvider,
    public alertCtrl: AlertController,
    public webService: WebserviceProvider
    ) {
  }

  takePicture(){
    this.cameraProv.takePictureGalery().then((data) => {
      this.webService.uploadImage(data).subscribe((data) => {
        console.log('enviada para api')
        if(data){
          this.alertPresent('Success', 'imagem salva com sucesso!')
        }
      })
    }).catch((err) => {
      console.log('deu erro da api', err)
      if(err == 20){
       this.alertPresent("Erro", "Conceda permissão de acesso sua camera para continuar")
      } else if (err == "No Image Selected"){
        console.log("nenhuma imagem selecionada")
      } else {
       this.alertPresent("Erro", "Ocorreu um erro, tente novamente")
      }
    })
  }

  teste(){
    console.log('click teste')
    this.webService.teste().subscribe((data)=> {
      console.log('result', data)
    })
  }

  alertPresent(title:string, message:string){
    this.alertCtrl.create({
      title: title,
      message: message
    }).present()
  }

  

 

}
