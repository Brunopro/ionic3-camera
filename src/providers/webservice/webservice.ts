import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class WebserviceProvider {

  url:string = 'http://192.168.0.26:8000/api/upload';

  url2:string = 'http://192.168.0.26:8000/api/teste';

  constructor(public http: HttpClient) {

  }

  teste(){

    let headers: HttpHeaders = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.get<any[]>(this.url2,{headers: headers})
  }


  
  uploadImage(image64){

    let body = {
      base64_image: image64
    }

    let headers: HttpHeaders = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.post<any[]>(this.url, body,{headers: headers})
  }


}
