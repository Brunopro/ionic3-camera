import { Injectable } from '@angular/core';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { AlertController } from 'ionic-angular';
import { normalizeURL } from 'ionic-angular';

@Injectable()
export class CameraProvider {

  base64Image:any;

  constructor(
    public camera: Camera,
    public alertCtrl: AlertController) {
    console.log('Hello CameraProvider Provider');
  }

  takePictureGalery():Promise<String>{

    return new Promise((resolve, reject) => {
      let opcoesDeCameraLibrary: CameraOptions = {
        cameraDirection: 0,
        correctOrientation: false,
        destinationType: 0,
        encodingType: 1,
        quality: 80,
        saveToPhotoAlbum: true,
        sourceType: 0
      }
    
      
      this.camera.getPicture(opcoesDeCameraLibrary).then((imageData) => {
       // imageData is either a base64 encoded string or a file URI
       // If it's base64 (DATA_URL):
       this.base64Image = 'data:image/png;base64,' + imageData;
       this.base64Image = normalizeURL(this.base64Image);
  
       this.encodeImageUri(this.base64Image, function (image64) {
         console.log('image64   ',image64)
         reject(image64)
       })
  
  
      }, err => {
       // Handle error
       reject(err);
      });
    })
    
  }

   //onTakePicture and openImagePickerCrop
   encodeImageUri(imageUri, callback) {
    console.log('execute encodeImageUri')
    var c = document.createElement('canvas');
    var ctx = c.getContext("2d");
    var img = new Image();
    img.onload = function () {
      var aux: any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      var dataURL = c.toDataURL("image/jpeg");
      callback(dataURL);
    };
    img.src = imageUri;
  };


  alertPresent(title: string, message: string){
    this.alertCtrl.create({
      title: title,
      message: message
    }).present()
  }

}
